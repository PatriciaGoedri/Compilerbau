import java.util.*;

/*
 * @author Herbert Bärschneider
 */
public class DFAGenerator {

    private SortedMap<DFAState, Map<Character, DFAState>> stateTransitionTable = new TreeMap<>();

    public SortedMap<DFAState, Map<Character, DFAState>> generateStateTransitionTable(SortedMap<Integer, FollowposTableEntry> followposTable) {
        int pos = 1;
        ArrayList<DFAState> unmarked = new ArrayList<>();
        ArrayList<DFAState> marked = new ArrayList<>();
        // initialser Zustand
        FollowposTableEntry entry = followposTable.get(pos);
        DFAState state = new DFAState(pos++, false, new HashSet<>());
        state.positionsSet.addAll(entry.followpos);
        unmarked.add(state);
        // Iteration über unmarkierte Zustände
        while(!unmarked.isEmpty()) {
            state = unmarked.get(0);
            marked.add(state);
            unmarked.remove(0);
            // anstatt alle theoretisch möglichen Zeichen zu durchlaufen, sammle ich aktuell möglichen Zeichen, um später darüber zu iterieren
            ArrayList<Character> possibleChars = new ArrayList<Character>();
            for(int i : state.positionsSet) {
                possibleChars.add(followposTable.get(i).symbol.toCharArray()[0]);
            }
            boolean acceptingState = possibleChars.contains('#');   // Entscheidung, ob Endzustand oder nicht
            if(possibleChars.contains('#')) possibleChars.remove('#');  // # ist kein eigentliches Zeichen, deswegen muss es wieder entfernt werden!
            // Iteration über die Eingabesysmbole
            for(char c : possibleChars) {
                HashSet<Integer> stateSet =  new HashSet<>();
                // Vereinigung der followpos für alle Positionen im State, welche mit dem char übereinstimmen
                for(int i : state.positionsSet) {
                    if(followposTable.get(i).symbol.toCharArray()[0]==c) stateSet.addAll(followposTable.get(i).followpos);
                }
                // Kontrolle, ob die Vereinigung schon bei den Zuständen eingetragen ist
                boolean contained = false;
                DFAState s = null;
                for(DFAState d : unmarked) {
                    if(d.positionsSet.equals(stateSet)) {
                        contained = true;
                        s = d;
                    }
                }
                for(DFAState d : marked) {
                    if(d.positionsSet.equals(stateSet)) {
                        contained = true;
                        s = d;
                    }
                }
                // Hinzufügen zu unmarkierte Zustände, falls Zustand noch nicht vorhanden
                if(!contained) {
                    s = new DFAState(pos++, acceptingState, new HashSet<>());
                    unmarked.add(s);
                    s.positionsSet.addAll(stateSet);
                }
                // Ergänzung der Zustandsübergangstabelle
                if(stateTransitionTable.containsKey(state)) {   // DFAState bereits in Zustandstabelle enthalten -> übergänge müssen nur ergänzt werden
                    stateTransitionTable.get(state).put(c, s);
                } else {
                    HashMap<Character, DFAState> transitions = new HashMap<>();
                    transitions.put(c, s);
                    stateTransitionTable.put(state, transitions);
                }
            }   // end of for(char ...)
        }   // end of while
        return stateTransitionTable;
    }

}
