import java.text.ParseException;

/*
 * @author Patricia Gödri
 */

public class TopDownParser {

    private String regEx;

    private int pos;


    public Visitable parse(String regEx) throws ParseException {
        this.regEx = regEx;
        return start();
    }

    private Visitable start() throws ParseException {
        if (regEx.charAt(0) == '#') { //'#' : Ende des Regulaeren Ausdrucks

            return new OperandNode("#");
        } else if (regEx.charAt(0) == '(') {   // '(' regexp ')' '#' : Erster Ableitungsschritt

            pos++;
            OperandNode leaf = new OperandNode("#");
            BinOpNode root = new BinOpNode("°", regExp(null), leaf);
            return root;
        }
        throw new ParseException("Falscher RegEx Start", pos);
    }

    private Visitable regExp(Visitable p) throws ParseException {
        char c = regEx.charAt(pos);
        if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '(') {  //TermRE'  : zweiter Ableitungsschritt

            return (re(term(null)));
        }
        throw new ParseException("Falscher RegEx Start", pos);
    }

    private Visitable re(Visitable p) throws ParseException {
        if (regEx.charAt(pos) == '|') { // RE -> '|' Term RE : dritter Ableitungsschritt

            pos++;
            BinOpNode root = new BinOpNode("|", p, term(null));
            return re(root);
        } else if (regEx.charAt(pos) == ')') {  // RE -> epsilon : dritter Ableitungsschritt (Ende des Regulaeren Ausdrucks)

            pos++;
            return p;
        }
        throw new ParseException("...", pos);
    }


    private Visitable term(Visitable p) throws ParseException {
        char c = regEx.charAt(pos);
        if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '(') { //Term -> FactorTerm : vierter Ableitungsschritt

            Visitable term1 = null;
            if (p != null) {
                BinOpNode root = new BinOpNode("°", p, factor(null));
                term1 = root;
            } else {
                term1 = factor(null);
            }
            return term(term1);
        } else if (c == '|' || c == ')') { //Term -> epsilon : vierter Ableitungsschritt (Ende des Regulaeren Ausdrucks)

            return p;
        }
        throw new ParseException("Kein Term", pos);
    }



    private Visitable factor(Visitable p) throws ParseException {
        char c = regEx.charAt(pos);
        if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '(') { //Faktor -> Elem HOp

            return hop(elem(null));
        }
        throw new ParseException("Kein Faktor", pos);
    }

    private Visitable hop(Visitable p) throws ParseException {
        char c = regEx.charAt(pos);
        if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '(' || c == '|' || c == ')') { //HOp -> epsilon

            return p;
        } else if (c == '*') { //HOp -> '*'

            pos++;
            return (new UnaryOpNode("*", p));
        } else if (c == '+') { //HOp -> '+'

            pos++;
            return new UnaryOpNode("+", p);
        } else if (c == '?') { //HOp -> '?'

            pos++;
            return new UnaryOpNode("?", p);
        }
        throw new ParseException("...", pos);
    }

    private Visitable elem(Visitable p) throws ParseException {
        char c = regEx.charAt(pos);
        if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z') { // Elem -> Alphanum

            return alphanum(null);
        } else if (c == '(') { // Elem ->  '(' regexp ')'

            pos++;
            return regExp(null);
        }
        throw new ParseException("Kein Element", pos);
    }

    private Visitable alphanum(Visitable p) throws ParseException {
        char c = regEx.charAt(pos);
        if (c >= '0' && c <= '9' || c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z') { // Alphanum -> '...'

            pos++;



          return new OperandNode(String.valueOf(c));

        }
        throw new ParseException("Kein Alphanummerum", pos);
    }

}