import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;

/*
 * @author Mirco Käsmann
 */
public class SecondVisitor implements Visitor{

    private SortedMap<Integer, FollowposTableEntry> followposTableEntries = new TreeMap<>();

    // sollte erst nach dem Durchlauf des Syntaxbaumes aufgerufen werden
    public SortedMap<Integer, FollowposTableEntry> getFollowposTableEntries() {
        return followposTableEntries;
    }

    @Override
    public void visit(OperandNode node) {
        FollowposTableEntry entry = new FollowposTableEntry(node.position, node.symbol);
        // entry.followpos = new HashSet<>();
        // nicht notwendig, da schon in der Implementation der Klasse FollowposTableEntry realisiert
        followposTableEntries.put(node.position, entry);
    }

    @Override
    public void visit(BinOpNode node) {
        if(node.operator.equals("|")) {
            return;
        }
        // if(node.operator.equals("°"))
        for(int i: ((SyntaxNode)node.left).lastpos) {
            FollowposTableEntry pos = followposTableEntries.get(i);
            pos.followpos.addAll(((SyntaxNode)node.right).firstpos);
        }
    }

    @Override
    public void visit(UnaryOpNode node) {
        if(node.operator.equals("?")) {
            return;
        }
        if(node.operator.equals("*") || node.operator.equals("+")) {
            for(int i: node.lastpos) {
                FollowposTableEntry pos = followposTableEntries.get(i);
                pos.followpos.addAll(node.firstpos);
            }
            return;
        }

    }

}
