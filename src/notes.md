# Notizen zum Quellcode

Die Aufgabe orientier sich am Drachenbuch Seite 209-216

https://books.google.de/books?id=pTKAwL64NkoC&printsec=frontcover&hl=de&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false

https://books.google.de/books?id=pTKAwL64NkoC&pg=PA215&lpg=PA215&dq=followpos+in+compiler&source=bl&ots=kxOdx7F9-G&sig=ACfU3U02suFSBt10k2h1jAiRuBBtFfpXcw&hl=de&sa=X&ved=2ahUKEwjkhZ2R_f_fAhVQsqQKHUUZBs4Q6AEwC3oECAQQAQ#v=onepage&q=followpos%20in%20compiler&f=false

### Anforderungen

* Top-Down-Parser
* 1. Visitor
* 2. Visitor
* DEA-Erzeuger
* Generischer Lexer

Da wir nur ein 4-Personen Team sind, müssen wir den generischen Lexer nicht bearbeiten

