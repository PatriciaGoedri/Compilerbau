import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/*
 * @author Patricia Gödri
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FirstVisitorTest
{

    private OperandNode n1;

    private OperandNode n2;

    private BinOpNode n3;

    private UnaryOpNode n4;

    private OperandNode n5;

    private BinOpNode n6;

    private OperandNode n7;

    private BinOpNode n8;

    private OperandNode n9;

    private BinOpNode n10;

    private OperandNode n11;

    private BinOpNode rootNode;



    @BeforeAll
    void createTree()
    {
        //1ter Knoten
        n1 = new OperandNode("a");

        //2ter Knoten
        n2 = new OperandNode("b");

        //3ter Knoten
        n3 = new BinOpNode("|", n1, n2);

        //4ter Knoten
        n4 = new UnaryOpNode("*", n3);

        //5ter Knoten
        n5 = new OperandNode("a");

        //6ter Knoten
        n6 = new BinOpNode("°", n4, n5);

        //7ter Knoten
        n7 = new OperandNode("b");

        //8ter Knoten
        n8 = new BinOpNode("°", n6, n7);

        //9ter Knoten
        n9 = new OperandNode("b");

        //10ter Knoten
        n10 = new BinOpNode("°", n8, n9);

        //11ter Knoten
        n11 = new OperandNode("#");

        //Root
        rootNode = new BinOpNode("°", n10, n11);

        //start
        DepthFirstIterator.traverse(rootNode, new FirstVisitor());
    }

    @Test
    void nullableTest()
    {
        assertFalse(n1.nullable);
        assertFalse(n2.nullable);
        assertFalse(n3.nullable);
        assertTrue(n4.nullable);
        assertFalse(n5.nullable);
        assertFalse(n6.nullable);
        assertFalse(n7.nullable);
        assertFalse(n8.nullable);
        assertFalse(n9.nullable);
        assertFalse(n10.nullable);
        assertFalse(n11.nullable);
        assertFalse(rootNode.nullable);
    }



    @Test
    void firstPosTest()
    {
        assertEquals(Set.of(1), n1.firstpos);
        assertEquals(Set.of(2), n2.firstpos);
        assertEquals(Set.of(1, 2), n3.firstpos);
        assertEquals(Set.of(1, 2),n4.firstpos);
        assertEquals(Set.of(3), n5.firstpos);
        assertEquals(Set.of(1, 2, 3), n6.firstpos);
        assertEquals(Set.of(4), n7.firstpos);
        assertEquals(Set.of(1, 2, 3), n8.firstpos);
        assertEquals(Set.of(5), n9.firstpos);
        assertEquals(Set.of(1, 2, 3), n10.firstpos);
        assertEquals(Set.of(6), n11.firstpos);
        assertEquals(Set.of(1, 2, 3), rootNode.firstpos);
    }



    @Test
    void lastPosTest()
    {
        assertEquals(Set.of(1), n1.lastpos);
        assertEquals(Set.of(2), n2.lastpos);
        assertEquals(Set.of(1, 2), n3.lastpos);
        assertEquals(Set.of(1, 2), n4.lastpos);
        assertEquals(Set.of(3), n5.lastpos);
        assertEquals(Set.of(3), n6.lastpos);
        assertEquals(Set.of(4), n7.lastpos);
        assertEquals(Set.of(4), n8.lastpos);
        assertEquals(Set.of(5), n9.lastpos);
        assertEquals(Set.of(5), n10.lastpos);
        assertEquals(Set.of(6), n11.lastpos);
        assertEquals(Set.of(6), rootNode.lastpos);
    }

    @Test
    void positionTest() {
        assertEquals(1, n1.position);
        assertEquals(2, n2.position);
        assertEquals(3, n5.position);
        assertEquals(4, n7.position);
        assertEquals(5, n9.position);
        assertEquals(6, n11.position);
    }

}
