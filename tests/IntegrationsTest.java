import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

/*
    @Author Anselm Grath
 */
import static org.junit.jupiter.api.Assertions.assertEquals;

public class IntegrationsTest {

    private String regEx;

    private SortedMap<DFAState, Map<Character, DFAState>> stateTransitionTable =  new TreeMap<>();

    @BeforeEach
    public void setUp() {
        regEx = "((a|b)*abb)#";
        DFAState s1 = new DFAState(1, false, Set.of(1, 2, 3));
        DFAState s2 = new DFAState(2, false, Set.of(1, 2, 3, 4));
        DFAState s3 = new DFAState(3, false, Set.of(1, 2, 3, 5));
        DFAState s4 = new DFAState(4, true, Set.of(1, 2, 3, 6));
        HashMap<Character, DFAState> m1 = new HashMap<>();
        m1.put('a', s2); m1.put('b', s1);
        HashMap<Character, DFAState> m2 = new HashMap<>();
        m2.put('a', s2); m2.put('b', s3);
        HashMap<Character, DFAState> m3 = new HashMap<>();
        m3.put('a', s2); m3.put('b', s4);
        HashMap<Character, DFAState> m4 = new HashMap<>();
        m4.put('a', null); m4.put('b', s1);
        stateTransitionTable.put(s1, m1);
        stateTransitionTable.put(s2, m2);
        stateTransitionTable.put(s3, m3);
        stateTransitionTable.put(s4, m4);
    }

    @Test
    public void integrationTest() {
        try {
            TopDownParser tdp = new TopDownParser();
            FirstVisitor first = new FirstVisitor();
            SecondVisitor second = new SecondVisitor();
            DFAGenerator gen = new DFAGenerator();
            Visitable root = tdp.parse(regEx);
            DepthFirstIterator.traverse(root, first);
            DepthFirstIterator.traverse(root, second);
            SortedMap<Integer, FollowposTableEntry> followPosTable = second.getFollowposTableEntries();
            SortedMap<DFAState, Map<Character, DFAState>> stateTransTable = gen.generateStateTransitionTable(followPosTable);
            assertEquals(stateTransitionTable, stateTransTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
