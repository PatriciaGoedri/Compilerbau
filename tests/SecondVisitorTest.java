import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.*;

/*
    @Author Anselm Grath
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SecondVisitorTest
{

    private SortedMap<Integer, FollowposTableEntry> followposTableEntries;

    private BinOpNode rootNode;

    @BeforeAll
	void setup()
	{
		//1ter Knoten
        OperandNode n1 = new OperandNode("a");
        n1.position = 1;
        n1.firstpos.addAll(Set.of(1));
        n1.lastpos.addAll(Set.of(1));

        //2ter Knoten
        OperandNode n2 = new OperandNode("b");
        n2.position = 2;
        n2.firstpos.addAll(Set.of(2));
        n2.lastpos.addAll(Set.of(2));

        //3ter Knoten
        BinOpNode n3 = new BinOpNode("|", n1, n2);
        n3.firstpos.addAll(Set.of(1, 2));
        n3.lastpos.addAll(Set.of(1, 2));

        //4ter Knoten
		UnaryOpNode n4 = new UnaryOpNode("*", n3);
        n4.firstpos.addAll(Set.of(1, 2));
        n4.lastpos.addAll(Set.of(1, 2));

		//5ter Knoten
        OperandNode n5 = new OperandNode("a");
		n5.position = 3;
        n5.firstpos.addAll(Set.of(3));
        n5.lastpos.addAll(Set.of(3));

		//6ter Knoten
        BinOpNode n6 = new BinOpNode("°", n4, n5);
        n6.firstpos.addAll(Set.of(1, 2, 3));
        n6.lastpos.addAll(Set.of(3));

		//7ter Knoten
        OperandNode n7 = new OperandNode("b");
		n7.position = 4;
        n7.firstpos.addAll(Set.of(4));
        n7.lastpos.addAll(Set.of(4));

		//8ter Knoten
        BinOpNode n8 = new BinOpNode("°", n6, n7);
        n8.firstpos.addAll(Set.of(1, 2, 3));
        n8.lastpos.addAll(Set.of(4));

        //9ter Knoten
        OperandNode n9 = new OperandNode("b");
		n9.position = 5;
        n9.firstpos.addAll(Set.of(5));
        n9.lastpos.addAll(Set.of(5));

		//10ter Knoten
        BinOpNode n10 = new BinOpNode("°", n8, n9);
        n10.firstpos.addAll(Set.of(1, 2, 3));
        n10.lastpos.addAll(Set.of(5));

		//11ter Knoten
		OperandNode n11 = new OperandNode("#");
		n11.position = 6;
        n11.firstpos.addAll(Set.of(6));
        n11.lastpos.addAll(Set.of(6));

		//Root
		rootNode = new BinOpNode("°", n10, n11);
        rootNode.firstpos.addAll(Set.of(1, 2, 3));
        rootNode.lastpos.addAll(Set.of(6));

		followposTableEntries = new TreeMap<>();
		FollowposTableEntry e1 =  new FollowposTableEntry(1, "a");
        e1.followpos.addAll(Set.of(1, 2, 3));
        FollowposTableEntry e2 =  new FollowposTableEntry(2, "b");
        e2.followpos.addAll(Set.of(1, 2, 3));
        FollowposTableEntry e3 =  new FollowposTableEntry(3, "a");
        e3.followpos.addAll(Set.of(4));
        FollowposTableEntry e4 =  new FollowposTableEntry(4, "b");
        e4.followpos.addAll(Set.of(5));
        FollowposTableEntry e5 =  new FollowposTableEntry(5, "b");
        e5.followpos.addAll(Set.of(6));
        FollowposTableEntry e6 =  new FollowposTableEntry(6, "#");
        followposTableEntries.put(1, e1);
        followposTableEntries.put(2, e2);
        followposTableEntries.put(3, e3);
        followposTableEntries.put(4, e4);
        followposTableEntries.put(5, e5);
        followposTableEntries.put(6, e6);
	}

	@Test
    void followPosTest(){
        SecondVisitor second = new SecondVisitor();
        DepthFirstIterator.traverse(rootNode, second);
        SortedMap<Integer, FollowposTableEntry> followposTable = second.getFollowposTableEntries();
        //assertEquals(followposTableEntries, followposTable);
        for(int i = 1; i <= followposTable.lastKey(); i++) {
            FollowposTableEntry expected = followposTableEntries.get(i);
            FollowposTableEntry actual = followposTable.get(i);
            assertEquals(expected.position, actual.position);
            assertEquals(expected.symbol, actual.symbol);
            assertEquals(expected.followpos, actual.followpos);
        }
    }

}