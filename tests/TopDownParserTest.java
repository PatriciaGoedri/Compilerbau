import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*
 * @author Herbert Bärschneider
 */
class TopDownParserTest {

    private TopDownParser tdp;

    private String regEx;

    private BinOpNode syntaxTree;

    @BeforeEach
    void setUp() {
         regEx = "((a?)z)#";
         tdp = new TopDownParser();
         syntaxTree =
                 new BinOpNode("°",
                    new BinOpNode("°",
                         new UnaryOpNode("?",
                                 new OperandNode("a")),
                         new OperandNode("z")),
                    new OperandNode("#"));
    }

    @Test
    void parse() {
        try {
            BinOpNode root = (BinOpNode) tdp.parse(regEx);
            SyntaxNode traveler = root;
            SyntaxNode trvlr = syntaxTree;
            do {
                if(traveler instanceof BinOpNode) {
                    assertEquals(trvlr.getClass(), traveler.getClass());
                    assertEquals(((BinOpNode) trvlr).left.getClass(), ((BinOpNode) traveler).left.getClass());
                    assertEquals(((BinOpNode) trvlr).right.getClass(), ((BinOpNode) traveler).right.getClass());
                    assertEquals(((BinOpNode) trvlr).operator, ((BinOpNode) traveler).operator);
                    if(((BinOpNode) traveler).left instanceof OperandNode) {
                        assertEquals(((OperandNode) ((BinOpNode) trvlr).left).symbol,((OperandNode) ((BinOpNode) traveler).left).symbol);
                    }
                    if(((BinOpNode) traveler).right instanceof OperandNode) {
                        assertEquals(((OperandNode) ((BinOpNode) trvlr).right).symbol,((OperandNode) ((BinOpNode) traveler).right).symbol);
                    }
                    traveler = (SyntaxNode) ((BinOpNode) traveler).left;
                    trvlr = (SyntaxNode) ((BinOpNode) trvlr).left;
                    continue;
                }
                if(traveler instanceof UnaryOpNode) {
                    assertEquals(trvlr.getClass(), traveler.getClass());
                    assertEquals(((UnaryOpNode) traveler).subNode.getClass(), ((UnaryOpNode) traveler).subNode.getClass());
                    assertEquals(((UnaryOpNode) trvlr).operator, ((UnaryOpNode) traveler).operator);
                    if(((UnaryOpNode) traveler).subNode instanceof OperandNode) {
                        assertEquals(((OperandNode) ((UnaryOpNode) trvlr).subNode).symbol,((OperandNode) ((UnaryOpNode) traveler).subNode).symbol);
                    }
                    traveler = (SyntaxNode) ((UnaryOpNode) traveler).subNode;
                    trvlr = (SyntaxNode) ((UnaryOpNode) trvlr).subNode;
                    continue;
                }
            } while(!(traveler instanceof OperandNode));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}